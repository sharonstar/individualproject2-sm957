# IndividualProject2-sm957

**Requirements:Continuous Delivery of Rust Microservice**
* Simple REST API/web service in Rust
* Dockerfile to containerize service
* CI/CD pipeline files

**Author:**   Sharon Ma

**Video link:**

Demo Video:  https://youtu.be/mt1pVJ-zQ38
# Steps

- Create Simple REST API/web service. The functionality of the service is to calculate the area of a circle given the radius.
```
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::Deserialize;
use std::f64::consts::PI;

// Derive Deserialize for FruitQuantities
#[derive(Deserialize)]
pub struct Request {
    pub r: f64,
}

async fn cal_area(req: web::Path<Request>) -> impl Responder {
    let radius = req.r;
    let area = PI * radius.powi(2);

    HttpResponse::Ok().body(format!("The area is {:.4}.", area))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/radius/{r}", web::get().to(cal_area))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
```

- Write Dockerfile to containerize the service

- Build the Docker Image and create the Docker containerize

   `docker build -t yourimagename .`

    `docker run -p 8080:8080 yourimagename`

- Visit localhost:8080 to test your service

- Push your code to GitLab and set up the GitLab CI/CD pipeline. Then create the .gitlab-ci.yml file.

```
stages:
  - build

variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_PROJECT_PATH:$CI_COMMIT_REF_SLUG

build:
  stage: build
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t microservice .
    - docker run -d -p 8080:8080 microservice
    - docker ps -a
  only:
    - main
    - merge_requests

```
# Screenshot
- Build the docker image

![](pic/pic1.png)

- View all the docker images

![](pic/pic2.png)

- Run the docker container

![](pic/pic3.png)

- Pipeline shows the GitLab workflow

![](pic/pic5.png)

- Test your service in localhost:8080

![](pic/pic4.png)
