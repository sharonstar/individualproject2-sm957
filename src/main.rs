use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::Deserialize;
use std::f64::consts::PI;

// Derive Deserialize for FruitQuantities
#[derive(Deserialize)]
pub struct Request {
    pub r: f64,
}

async fn cal_area(req: web::Path<Request>) -> impl Responder {
    let radius = req.r;
    let area = PI * radius.powi(2);

    HttpResponse::Ok().body(format!("The area is {:.4}.", area))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/radius/{r}", web::get().to(cal_area))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
